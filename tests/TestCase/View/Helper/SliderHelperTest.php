<?php
namespace Slideshow\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Slideshow\View\Helper\SliderHelper;

/**
 * Slideshow\View\Helper\SliderHelper Test Case
 */
class SliderHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Slideshow\View\Helper\SliderHelper
     */
    public $Slider;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Slider = new SliderHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Slider);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
