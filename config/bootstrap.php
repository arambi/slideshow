<?php 
use Section\Action\ActionCollection;
use Manager\Navigation\NavigationCollection;
use User\Auth\Access;
use Block\Lib\BlocksRegistry;
 



Access::add( 'slideshow', [
  'name' => 'Slideshow',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slideshows',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slideshows',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slideshows',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slideshows',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slideshows',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slides',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slides',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slides',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slides',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slides',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slides',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Slideshow',
          'controller' => 'Slides',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Slideshows',
  'parentName' => 'Slideshows',
  'plugin' => 'Slideshow',
  'controller' => 'Slideshows',
  'action' => 'index',
  'icon' => 'fa fa-film',
]);







// Bloque Slider
BlocksRegistry::add( 'slideshow', [
    'key' => 'slideshow',
    'title' => __d( 'admin', 'Slider'),
    'icon' => 'fa fa-film',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Slideshow\\Model\\Block\\SlideshowBlock',
    'cell' => 'Slideshow.Slideshow::display',
    'blockView' => 'Slideshow/blocks/slideshow'
]);
