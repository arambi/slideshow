<?php

use Phinx\Migration\AbstractMigration;

class Nulls extends AbstractMigration
{

  public function change()
  {
    $slides = $this->table( 'slideshow_slides');
    $slides
      ->changeColumn( 'title', 'string', array( 'limit' => 255, 'null' => true, 'default' => null))
      ->changeColumn( 'subtitle', 'string', array( 'limit' => 255, 'null' => true, 'default' => null))
      ->changeColumn( 'subtitle2', 'string', array( 'limit' => 255, 'null' => true, 'default' => null))
      ->changeColumn( 'button_text', 'string', array( 'limit' => 64, 'null' => true, 'default' => null))
      ->changeColumn( 'button_link', 'string', array( 'limit' => 255, 'null' => true, 'default' => null))
      ->changeColumn( 'url', 'string', array( 'limit' => 255, 'null' => true, 'default' => null))
      ->save();
  }
}
