<?php
use Migrations\AbstractMigration;

class SlidePosition extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $slides = $this->table( 'slideshow_slides');
    $slides
      ->changeColumn( 'title', 'text', array( 'default' => null))
      ->changeColumn( 'subtitle', 'text', array( 'default' => null))
      ->addColumn( 'position', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
      ->update();
  }
}
