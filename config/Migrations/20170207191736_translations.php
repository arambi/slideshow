<?php

use Phinx\Migration\AbstractMigration;

class Translations extends AbstractMigration
{
  public function change()
  {
    $contents = $this->table( 'slideshow_slides_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $contents
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', [ 'limit' => 255, 'default' => null])
      ->addColumn( 'subtitle', 'string', [ 'limit' => 255, 'default' => null])
      ->addColumn( 'subtitle2', 'string', [ 'limit' => 255, 'default' => null])
      ->addColumn( 'button_text', 'string', [ 'limit' => 64, 'default' => null])
      ->addColumn( 'button_link', 'string', [ 'limit' => 255, 'default' => null])
      ->addColumn( 'url', 'string', [ 'limit' => 255, 'default' => null])
      ->save();  
  }
}
