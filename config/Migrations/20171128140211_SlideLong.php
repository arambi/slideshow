<?php
use Migrations\AbstractMigration;

class SlideLong extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $contents = $this->table( 'slideshow_slides_translations');
    $contents
      ->changeColumn( 'title', 'text', [ 'null' => true, 'default' => null])
      ->changeColumn( 'subtitle', 'text', [ 'null' => true, 'default' => null])
      ->update();  
  }
}
