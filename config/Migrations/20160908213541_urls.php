<?php

use Phinx\Migration\AbstractMigration;

class Urls extends AbstractMigration
{

  public function change()
  {
    $slides = $this->table( 'slideshow_slides');
    $slides
      ->addColumn( 'docs', 'text', ['default' => NULL, 'null' => true])
      ->addColumn( 'parent_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'url', 'string', array( 'limit' => 255, 'default' => null))
      ->addIndex( ['parent_id'])
      ->save();
  }
}
