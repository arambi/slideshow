<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
  public function change()
  {
    $sliders = $this->table( 'slideshow_slideshows');
    $sliders
      ->addColumn( 'title', 'string', array( 'limit' => 64, 'default' => null))
      ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'height', 'integer', ['default' => null, 'null' => true])
      ->addColumn( 'width', 'integer', ['default' => null, 'null' => true])
      ->addColumn( 'auto_height', 'boolean', ['null' => true, 'default' => 0])
      ->addColumn( 'layout', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
      ->addColumn( 'view', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))

      ->addColumn( 'parallax_mode', 'string', array( 'limit' => 16, 'null' => true, 'default' => null))
      ->addColumn( 'speed', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
      ->addColumn( 'dir', 'string', array( 'limit' => 4, 'null' => true, 'default' => null))
      ->addColumn( 'autoplay', 'boolean', array( 'null' => true, 'default' => null))
      ->addColumn( 'slider_loop', 'boolean', array( 'null' => true, 'default' => null))
      ->addColumn( 'shuffle', 'boolean', array( 'null' => true, 'default' => null))

      ->addColumn( 'created', 'datetime', array('default' => null))
      ->addColumn( 'modified', 'datetime', array('default' => null))
      ->addIndex( ['salt'])
      ->save();

    $slides = $this->table( 'slideshow_slides');
    $slides
      ->addColumn( 'slider_id', 'integer', ['null' => true, 'default' => null])
      ->addColumn( 'title', 'string', array( 'limit' => 255, 'default' => null))
      ->addColumn( 'subtitle', 'string', array( 'limit' => 255, 'default' => null))
      ->addColumn( 'subtitle2', 'string', array( 'limit' => 255, 'default' => null))
      ->addColumn( 'text_align', 'string', array( 'limit' => 32, 'default' => null))
      ->addColumn( 'fontsize', 'integer', array( 'limit' => 4, 'null' => true, 'default' => null))
      ->addColumn( 'with_button', 'boolean', array( 'null' => true, 'default' => null))
      ->addColumn( 'button_text', 'string', array( 'limit' => 64, 'default' => null))
      ->addColumn( 'button_link', 'string', array( 'limit' => 255, 'default' => null))
      ->addColumn( 'color', 'string', array( 'limit' => 16, 'default' => null))
      ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])
      ->addColumn( 'photo', 'text', array( 'null' => true, 'default' => null))
      ->addColumn( 'background', 'text', array( 'null' => true, 'default' => null))
      ->addColumn( 'published', 'boolean', array( 'null' => true, 'default' => null))
      ->addColumn( 'shadow', 'boolean', array( 'null' => true, 'default' => null))
      ->addColumn( 'created', 'datetime', array( 'default' => null))
      ->addColumn( 'modified', 'datetime', array('default' => null))
      ->addColumn( 'title_origin', 'string', array( 'limit' => 32, 'default' => null))
      ->addColumn( 'subtitle_origin', 'string', array( 'limit' => 32, 'default' => null))
      ->addColumn( 'subtitle2_origin', 'string', array( 'limit' => 32, 'default' => null))
      ->addColumn( 'button_origin', 'string', array( 'limit' => 32, 'default' => null))
      ->addColumn( 'settings', 'text', array( 'default' => null))
      ->addIndex( ['slider_id'])
      ->addIndex( ['salt'])
      ->save();
  }
}
