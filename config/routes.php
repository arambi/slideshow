<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'Slideshow',
    ['path' => '/slideshow'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);
