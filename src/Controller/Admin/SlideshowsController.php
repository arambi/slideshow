<?php
namespace Slideshow\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Slideshow\Controller\AppController;

/**
 * Slider Controller
 *
 * @property \Slideshow\Model\Table\SliderTable $Slider
 */
class SlideshowsController extends AppController
{
    use CrudControllerTrait;
}
