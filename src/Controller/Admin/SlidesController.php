<?php
namespace Slideshow\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Slideshow\Controller\AppController;

/**
 * Slides Controller
 *
 * @property \Slideshow\Model\Table\SlidesTable $Slides
 */
class SlidesController extends AppController
{
    use CrudControllerTrait;
}
