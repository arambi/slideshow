<?php $id = rand(0,999999) ?>
<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider_<?= $id ?>">
    <?php foreach( $slider->slides as $slide): ?>
      <div class="ms-slide  master-slider-align-<?= $slide->text_align ?>" data-fill-mode="fill">     
          <!-- slide background -->
          <?php if( !empty( $slide->background)): ?>
              <?= $this->Uploads->image( $slide->background, 'org', [
                'data-src' => $slide->background->paths->org
              ]) ?>
          <?php endif ?>                    

          <div class="container slider-container">
            <?php if( !empty( $slide->photo)): ?>
              <div class="ms-layer ms-caption ms-image"
                  <?= !empty( $slide->settings->subtitle_effect) ? 'data-effect="'. $slide->settings->image_effect .'"' : '' ?>
                  <?= !empty( $slide->settings->subtitle_delay) ? 'data-delay="'. $slide->settings->image_delay .'"' : '' ?>
                  >
                <?= $this->Uploads->image( $slide->photo, 'org') ?>
              </div>
            <?php endif ?>
            <div  class="ms-content">
              <?php if( !empty( $slide->title)): ?>
                <div class="ms-layer ms-caption ms-content-title ms-align-<?= $slide->text_align ?>" 
                  <?= !empty( $slide->settings->title_effect) ? 'data-effect="'. $slide->settings->title_effect .'"' : '' ?> 
                  >
                  <h3 class="<?= $slide->shadow ? 'ms-shadow' : '' ?>">
                    <?= strip_tags( $slide->title, '<span>')  ?>
                  </h3>
                </div>
              <?php endif ?>

              <?php if( !empty( $slide->subtitle)): ?>
                <div class="ms-layer ms-caption ms-content-subtitle ms-align-<?= $slide->text_align ?>"
                  <?= !empty( $slide->settings->subtitle_effect) ? 'data-effect="'. $slide->settings->subtitle_effect .'"' : '' ?>
                  <?= !empty( $slide->settings->subtitle_delay) ? 'data-delay="'. $slide->settings->subtitle_delay .'"' : '' ?>
                  >
                  <h4><span><?= $slide->subtitle ?></span></h4>
                </div>
              <?php endif ?>

              <?php if( !empty( $slide->subtitle2)): ?>
                <div class="ms-layer ms-caption ms-content-subtitle2 ms-align-<?= $slide->text_align ?>"
                  <?= !empty( $slide->settings->subtitle_effect) ? 'data-effect="'. $slide->settings->subtitle2_effect .'"' : '' ?>
                  <?= !empty( $slide->settings->subtitle_delay) ? 'data-delay="'. $slide->settings->subtitle2_delay .'"' : '' ?>
                  >
                  <h5><span style="<?= !empty( $slide->settings->subtitle2_color) ? 'color: '. $slide->settings->subtitle2_color : '' ?>"><?= $slide->subtitle2 ?></span></h5>
                </div>
              <?php endif ?>

              <?php if( !empty( $slide->with_button)): ?>
                <div class="ms-layer ms-caption ms-layer-arflu ms-content-button ms-align-<?= $slide->text_align ?>"
                  <?= !empty( $slide->settings->button_effect) ? 'data-effect="'. $slide->settings->button_effect .'"' : '' ?>
                  <?= !empty( $slide->settings->button_delay) ? 'data-delay="'. $slide->settings->button_delay .'"' : '' ?>
                  >
                  <div class="ms-button">
                      <a class="ms-button" href="<?= $slide->button_link ?>"><?= $slide->button_text ?></a>
                  </div>
                </div>
              <?php endif ?>
            </div>
          </div>
            
      </div> 
    <?php endforeach ?>
      
    <!-- end of slide -->
</div>

<?php $this->Buffer->start() ?>

<script type="text/javascript">
  $('#masterslider_<?= $id ?>').masterslider({
    width: 1200,
    height: 450,
    // more options...
    autoHeight: false,
    autoplay: true,
    loop: true,
    shuffle: false,
    view: 'basic',
    layout: 'fullwidth'
    <?php if( count( $slider->slides) > 1): ?>
        , controls : {
          arrows : {autohide:false},
          bullets : {
            autohide: false
          }
        }
    <?php else: ?>
      , swipe: false
    <?php endif ?>
    
    
  });
</script>

<?php $this->Buffer->end() ?>