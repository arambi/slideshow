<?php
namespace Slideshow\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Slider helper
 */
class SliderHelper extends Helper
{

  public function effect( $entity, $field)
  {
    $prop = $field . '_effect';

    if( !empty( $entity->settings->$prop))
    {
      return 'data-effect="' . $entity->settings->$prop . '"';
    }
  }

  public function delay( $entity, $field)
  {
    $prop = $field . '_delay';

    if( !empty( $entity->settings->$prop))
    {
      return 'data-delay="' . $entity->settings->$prop . '"';
    }
  }
}
