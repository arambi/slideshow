<?php
namespace Slideshow\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
/**
 * Slideshow cell
 */
class SlideshowCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $query = TableRegistry::get( 'Slideshow.Slideshows')->find();
    $query->contain([
      'Slides' => function( $q){
        return $q->where( ['Slides.published' => 1]);
      }
    ]);
    $query->where( [
      'Slideshows.id' => $block->parent_id,
      
    ]);
    $slider = $query->first();
    $this->set( compact( 'slider', 'block'));
  }
}
