<?php
namespace Slideshow\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Blog\Model\Entity\BlogEntityTrait;

class Slide extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;
  use BlogEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'position' => false,
  ];

  protected function _getLink( $url)
  {
    if( $url !== null)
    {
      return $url;
    }

    if( empty( $this->settings->link_type))
    {
      return null;
    }

    if( $this->settings->link_type == 'section')
    {
      $url = TableRegistry::get( 'Section.Sections')->getUrl( $this->parent_id);
    }
    elseif( $this->settings->link_type == 'url')
    {
      $url = $this->url;
    }
    elseif( $this->settings->link_type == 'doc' || $this->settings->link_type == 'file')
    {
      $url = $this->docs->paths [0];
    }

    return $url;
  }

  protected function _getIsVideoUrl()
  {
    $url = $this->link;
    return strpos( $url, 'yout') !== false || strpos( $url, 'vimeo') !== false;
  }

  protected function _getVideoUrl()
  {
    $url = $this->link;

    if( strpos( $url, 'vimeo') !== false)
    {
      $parse = parse_url( $url);
      $path = $parse ['path'];
      $parts = explode( '/', $path);
      $id = end( $parts);
      return 'https://player.vimeo.com/video/'. $id;
    }
  }

  protected function _getVideoProvider()
  {
    $url = $this->link;
    
    if( strpos( $url, 'yout') !== false)
    {
      return 'youtube';
    }

    if( strpos( $url, 'vimeo') !== false)
    {
      return 'vimeo';
    }
  }


}
