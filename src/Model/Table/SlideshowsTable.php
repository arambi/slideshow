<?php
namespace Slideshow\Model\Table;

use Slideshow\Model\Entity\Slideshow;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sliders Model
 */
class SlideshowsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( 'slideshow_slideshows');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    // Asociations
    $this->hasMany( 'Slides', [
      'className' => 'Slideshow.Slides',
      'foreignKey' => 'slider_id'
    ]);

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
     $this->crud->associations( ['Slides']);
    

    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Título'),
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Sliders'),
        'plural' => __d( 'admin', 'Sliders'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'slides' => [
                    'label' => 'Slides',
                    'type' => 'hasMany'
                  ]
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
    ;
    
  }
}
