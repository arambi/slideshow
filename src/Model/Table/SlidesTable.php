<?php
namespace Slideshow\Model\Table;

use Slideshow\Model\Entity\Slide;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Manager\Traits\LinksConfigTrait;
use Cake\Core\Configure;

/**
 * Slides Model
 */
class SlidesTable extends Table
{
  use LinksConfigTrait;

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table( 'slideshow_slides');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    $this->addBehavior( 'Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Sortable');
    $this->addBehavior( 'Upload.UploadJsonable', [
      'fields' => [
        'photo',
        'background',
        'docs',
        'video',
      ]
    ]);

    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => [ 'settings']
    ]);

    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title', 'subtitle', 'subtitle2', 'button_text', 'button_link', 'url']
    ]);

    $effects = [
      'fade(500,true)' => 'Fundido',
      'top(500,true)' => 'Desde arriba',
      'left(500,true)' => 'Desde la izquierda',
      'right(500,true)' => 'Desde la derecha',
      'bottom(500,true)' => 'Desde abajo',
    ];

    $elements_general = [
      'published',
    ];

    if( !Configure::read( 'Slides.not_align'))
    {
      $elements_general [] = 'text_align';
    }

    $elements_general [] = 'background';
    


    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Título'),
          'type' => 'text',
        ],
        'published' => __d( 'admin', 'Publicado'),
        'settings.delay' => [
          'label' => 'Tiempo de permanencia',
          'type' => 'numeric',
          'range' => [0, 10],
          'help' => 'Segundos que estará el slide visible' 
        ],
        'subtitle' => [
          'label' => __d( 'admin', 'Subtítulo'),
          'type' => 'ckeditor',
        ],
        'subtitle2' => [
          'label' => __d( 'admin', 'Antetítulo'),
          'type' => 'ckeditor',
        ],
        'settings.title_effect' => [
          'label' => 'Efectos del título',
          'type' => 'select',
          'options' => $effects
        ],
        'settings.subtitle_effect' => [
          'label' => 'Efectos del subtítulo',
          'type' => 'select',
          'options' => $effects
        ],
        'settings.subtitle2_effect' => [
          'label' => 'Efectos del antetítulo',
          'type' => 'select',
          'options' => $effects
        ],
        'settings.button_effect' => [
          'label' => 'Efectos del botón',
          'type' => 'select',
          'options' => $effects,
          'show' => 'data.content.with_button'
        ],
        'settings.image_effect' => [
          'label' => 'Efectos de la imagen',
          'type' => 'select',
          'options' => $effects,
        ],
        'settings.title_delay' => [
          'label' => 'Retraso del título',
          'type' => 'numeric',
          'range' => [0, 2000, 100],
          'default' => 300,
          'help' => 'Tiempo de retraso en aparecer el elemento (en milésimas de segundo)' 
        ],
        'settings.subtitle_delay' => [
          'label' => 'Retraso del subtítulo',
          'type' => 'numeric',
          'range' => [0, 2000, 100],
          'help' => 'Tiempo de retraso en aparecer el elemento (en milésimas de segundo)' 
        ],
        'settings.subtitle2_delay' => [
          'label' => 'Retraso del antetítulo',
          'type' => 'numeric',
          'range' => [0, 2000, 100],
          'help' => 'Tiempo de retraso en aparecer el elemento (en milésimas de segundo)' 
        ],
        'settings.image_delay' => [
          'label' => 'Retraso de la imagen',
          'type' => 'numeric',
          'range' => [0, 2000, 100],
          'help' => 'Tiempo de retraso en aparecer el elemento (en milésimas de segundo)' 
        ],
        'settings.button_delay' => [
          'label' => 'Retraso del botón',
          'type' => 'numeric',
          'range' => [0, 2000, 100],
          'help' => 'Tiempo de retraso en aparecer el elemento (en milésimas de segundo)',
          'show' => 'data.content.with_button'
        ],
        'text_align' => [
          'label' => __d( 'admin', 'Posición del texto'),
          'type' => 'select',
          'options' => [
            'left' => 'Izquierda',
            'center' => 'Centrado',
            'right' => 'Derecha'
          ]
        ],
        'with_button' => [
          'label' => __d( 'admin', 'Con botón')
        ],
        'button_text' => [
          'label' => __d( 'admin', 'Texto del botón'),
          'show' => 'data.content.with_button'
        ],
        'color' => [
          'type' => 'colorpicker',
          'label' => __d( 'admin', 'Color del texto'),
        ],
        'shadow' => [
          'label' => __d( 'admin', 'Con sombra')
        ],
        'photo' => [
          'type' => 'upload',
          'label' => 'Foto',
          'config' => [
            'type' => 'slide',
            'size' => 'thm'
          ]
        ],
        'background' => [
          'type' => 'upload',
          'label' => 'Imagen de fondo',
          'config' => [
            'type' => 'slide',
            'size' => 'thm'
          ]
        ],
        // Enlace
        'settings.link_type' => [
          'label' => 'Tipo de enlace',
          'type' => 'select',
          'options' => [
            'section' => 'Una sección',
            'url' => 'Una URL',
            'file' => 'Un fichero'
          ],
          'show' => "content.with_button"
        ],
        'settings.target_blank' => [
          'label' => 'Abrir en una ventana nueva',
          'type' => 'boolean',
          'show' => "content.with_button"
        ],
        'url' => [
          'label' => __d( 'admin', 'URL'),
          'type' => 'string',
          'show' => "content.settings.link_type == 'url' && content.with_button"
        ],
        'parent_id' => [
          'label' => __d( 'admin', 'Enlace a sección'),
          'type' => 'select',
          'options' => function( $crud){
            return TableRegistry::get( 'Section.Sections')->selectOptions();
          },
          'show' => "content.settings.link_type == 'section' && content.with_button"
        ],
        'docs' => [
          'type' => 'upload',
          'label' => __d( 'admin', 'Fichero'),
          'config' => [
            'type' => 'doc',
          ],
          'show' => "content.settings.link_type == 'file' && content.with_button"
        ],
      ])
      ->addIndex( 'index', [
        'fields' => [
          'background',
          'title',
          'published',
        ],
        'actionButtons' => [],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Slides'),
        'plural' => __d( 'admin', 'Slides'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'key' => 'general',
            'cols' => 8,
            'box' => [
              [
                'elements' => $elements_general,
                'key' => 'general'
              ],
              
              [
                'title' => 'Título',
                'elements' => [
                  'subtitle2',
                  'title',
                  'subtitle',
                  'with_button',
                  'button_text',
                  'settings.link_type',
                  'url',
                  'docs',
                  'parent_id',
                  'settings.target_blank',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => []
      ], ['update'])
    ;


    $this->crud->defaults([
      'settings' => [
        'delay' => 4,
        'title_delay' => 200,
        'subtitle_delay' => 200,
        'subtitle2_delay' => 200,
        'button_delay' => 200,
        'image_delay' => 200,
        'title_effect' => 'fade(500,true)',
        'subtitle_effect' => 'fade(500,true)',
        'subtitle2_effect' => 'fade(500,true)',
        'button_effect' => 'fade(500,true)',
        'image_effect' => 'fade(500,true)',
      ]
    ]);
  }
}
